package test;

import java.util.HashMap;

public class App
{
    private HashMap<String, Integer> data = new HashMap<>();

    private void add(String key, int value) {
        data.put(key, value);
    }

    private void display(String key) {
        System.out.print(key + " - ");
        System.out.println(data.get(key));
    }

    public static void main( String[] args )
    {
    App clist = new App();
    clist.add("Boys", 5);
    clist.add("Girls", 7);
    clist.add("lgbt", 0);
    clist.display("Boys");
    clist.display("Girls");

//        HashMap<String, Integer> fruits = new HashMap();
//        fruits.put("Oranges", 5);
//        fruits.put("Apples", 10);
//        fruits.put("Grapes", 7);
//        HashMap<String, Integer> milkProducts = new HashMap();
//        milkProducts.put("Cheese", 2);
//        milkProducts.put("Milk", 3);
//        milkProducts.put("Jogurt", 17);
//        HashMap<String, HashMap<String, Integer> > eat = new HashMap();
//        eat.put("Fruits", fruits);
//        eat.put("Milks", milkProducts);
//        System.out.println(eat.get("Milks").get("Milk"));
    }
}

